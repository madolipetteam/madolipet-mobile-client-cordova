var FirstPageController = Class({
    firstPage: null,
    constructor: function()
    {
        this.firstPage = new FirstPage();
        this.firstPage.caller = this;
        this.firstPage.eventHandler = this.runAction;
    }
    ,
    start: function()
    {
        this.firstPage.display();
    }
    ,
    runAction: function(me, sender, command, param)
    {
        
    }
    
});