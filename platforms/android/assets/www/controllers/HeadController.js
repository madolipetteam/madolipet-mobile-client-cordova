var HeadController = Class( LoginController, {
    headPage: null,
    constructor: function()
    {
        //Call parent controller, to handle user login session
        HeadController.$super.call(this);
        
        //Create HomePage object
        this.headPage = new HeadPage();
        this.headPage.caller = this;
        this.headPage.eventHandler = this.headPageAction;
    }
    ,
    afterLogin: function(me)
    {
        console.log("wise : HeadController.afterLogin");
        this.headPage.display();
    }
    ,
    headPageAction: function(me, sender, command, param)
    {
        switch(command)
        {
            case "":
                
                break;
        }
    }
});