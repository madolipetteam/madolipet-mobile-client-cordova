var HomeController = Class(HeadController, {
    homePage: null,
    constructor: function()
    {
        HomeController.$super.call(this);
        
        //Create HomePage object
        this.homePage = new HomePage();
        this.homePage.caller = this;
        this.homePage.eventHandler = this.homePageAction;
    }
    ,
    afterLogin: function(me)
    {
        //Run HeadController's afterlogin
        HomeController.$superp.afterLogin.call(me, me);
        console.log("wise : HomeController.afterLogin");
        me.homePage.display();
    }
    ,
    homePageAction: function(me, sender, command, param)
    {
        switch(command)
        {
            case "get-and-display-forums":
                me.getAndDisplayForums(me, param);
            break;
            case "get-and-display-articles":
                me.getAndDisplayArticles(me, param);
            break;
        }
    }
    ,
    getAndDisplayForums: function(me, param)
    {
        var user = GLOBAL.userLogin;
        var forums = user.MemberOfs;
        me.homePage.displayForums(forums);
    }
    ,
    getArticles: function(me, param, callback)
    {
        ArticleHelper.getAllArticlesByForum(param, function(articles)
        {
            if(callback != null)
                callback(articles);
        });

    }
    ,
    displayArticles: function(me, articles)
    {
        this.homePage.displayArticles(articles);
    }
    ,
    getAndDisplayArticles: function(me, param)
    {
        me.getArticles(me, param, function( articles )
        {
            me.displayArticles(me, articles);
        });
    }

});