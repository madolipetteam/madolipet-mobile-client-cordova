var LoginController = Class({
    loginPage: null,
    constructor: function()
    {
        var me = this;
        me.loginPage = new LoginPage();
        me.loginPage.caller = me;
        me.loginPage.eventHandler = me.loginPageAction;
    }
    ,
    //This doesn't need to be called by child's Controller, child's controller only need to override
    //the afterLogin() function, the function called after start;
    start: function()
    {
        console.log("Checking current login");
        var me = this;
        MadolUserHelper.getCurrentLogin(
            function (user)
            {
                if(user == null)
                {
                    me.loginPage.display();
                }
                else
                {
                    GLOBAL.userLogin = user;
                    me.afterLogin(me);
                }
            });
    }
    ,
    loginPageAction: function(me, sender, command, param)
    {
        switch(command)
        {
            case "login":
                me.login(me, param);
                break;
        }
    }
    ,
    //Login function to handle btnLogin touch
    login: function(me, param)
    {
        
        console.log("Logging in with ---- " + JSON.stringify( param));
        MadolUserHelper.login(param,
            function (user)
            {
                if(user != null)
                {
                    GLOBAL.userLogin = user;
                    me.afterLogin(me);
                }
                else
                {
                    alert("Username or password is invalid");
                }
            });
    }
    ,
    
    //This function is to override by child controller
    afterLogin: function(me)
    {
        
    }
});