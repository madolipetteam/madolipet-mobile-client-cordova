var Config = {
    webserviceHost: 'http://madolipet.wiseape-enterprise.com',
    getUrl: function(command, getparam)
    {
        var sget = "";
        if(getparam != null)
        {
            for(var key in getparam)
            {
                sget += key + "=" + getparam[key] + "/";
            }
        }
        var url = Config.webserviceHost + "/index.php?path=" + command + "/";
        if(sget.length > 0)
            url += sget;
        url = url.substr(0, url.length - 1);
        return url;
    }
};

var GLOBAL =
{
    userLogin: null
}

