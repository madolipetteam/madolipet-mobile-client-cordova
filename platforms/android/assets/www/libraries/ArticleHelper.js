var ArticleHelper = {

    getAllArticlesByForum: function( idForum, callback )
    {
        var url = Config.getUrl("ArticleExt.getAllArticlesByForum", { IdForum : idForum });
        Util.get(url, null, function(data)
        {
            if(callback != null)
                callback(data.items);
        });
    }

};