var PageHandler = Class({
    previousPage: null,
    className: '',
    eventHandler: null,
    caller: null,
    persistentController: '',
    constructor: function(className)
    {
        this.className =  className;
        //this.pg =
    }
    ,
    display: function(callback)
    {
        var me = this;
        console.log("Display " + this.className);
        var selector = "div[controller=\"" + this.className + "\"]";
        //console.log(selector);
        this.hideAllWindows(this);
        me.previousPage = $(selector);
        $(selector).css("z-index", 1000);
        $(selector).show(1000, function()
        {
            console.log("showing " + selector);
            me.initPage(me);
            if(callback != null)
                callback();
        });
    }
    ,
    //This is function to override by child's page. This function is called after display.
    initPage: function(me)
    {
    
    }
    ,
    hideAllWindows: function(me)
    {
        var selector = "div[controller]";
        $(selector).each(function (idx)
            {
                var controller = $(this).attr("controller");
                console.log("controllers hides : " + controller + " - " + me.persistentController) ;
                if(controller != me.persistentController)
                {
                    $(this).css("z-index", 0);
                    $(this).hide();
                }
            });
    }
    ,
    runEventHandler: function(sender, command, param)
    {
        var me = this;
        console.log(me.eventHandler);
        if(me.eventHandler != null)
            me.eventHandler( me.caller, sender, command, param);
    }
    ,
    getDataObject: function(id)
    {
        var o = {};
        $("#" + id).find("*").each(
            function (idx)
            {
                if($(this).attr("datafield")  != null )
                {
                    console.log($(this).attr("datafield"));
                    var field = $(this).attr("datafield");
                    eval("o." + field + " = '" + $(this).val() + "'");
                }
            });

        return o;
    }
});