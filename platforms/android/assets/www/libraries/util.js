var Util = {
    animationCreated: false,
    startAnimation: function()
    {
        $("#appContainer").hide(1000);
        if(Util.animationImage != null && Util.animationCreated == false)
        {
            var img = $("<img>").attr("src", Util.animationImage ).css("z-index", 10000);
            img.css("position", "absolute");
            img.css("left", "10%");
            img.css("top", "20%");
            img.attr("id", "img-animator");
            $(document.body).append(img);
            Util.animationCreated = true;
        }

        $("#img-animator").show();

    }
    ,
    stopAnimation: function()
    {
        $("#img-animator").hide();
        $("#appContainer").show(1000);
    }
    ,
    post: function(url, param, callback)
    {
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            crossDomain: true,
            data: param,
            error: function(a, b, c)
            {
                console.log("Error.get.ajax : " + url + " ");
                console.log(JSON.stringify(a));
                console.log(b);
                console.log(c);
            }
            ,
            success: function(json)
            {
                try
                {
                    console.log("Response from server :")
                    console.log( JSON.stringify(json));
                    var o = json;
                    if(callback != null)
                    {
                        callback(o);
                    }
                }
                catch(e)
                {
                    console.log('Error.JSON.parse : ' + e.message);
                }
            }
        });
    }
    ,
    get: function(url, param, callback)
    {
        Util.startAnimation();
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'GET',
            crossDomain: true,
            data: param,
            error: function(a, b, c)
            {
                console.log("Error.get.ajax : " + url + " ");
                console.log(JSON.stringify(a));
                console.log(b);
                console.log(c);
            }
            ,
            success: function(json)
            {
                Util.stopAnimation();
                try
                {
                    console.log("Response from server :")
                    console.log( JSON.stringify( json));
                    var o = json;
                    if(callback != null)
                    {
                        callback(o);
                    }
                }
                catch(e)
                {
                    console.log('Error.JSON.parse : ' + e.message);
                }
            }
        });
    }
    ,
    animationImage: 'img/cat.gif'
    ,
    populateCombo: function(id, data, valueField, displayField, selectedValue)
    {
        $("#" + id).html("");
        for(var i = 0; i < data.length; i++)
        {
            var item = data[i];
            eval("var value = item." + valueField + ";");
            eval("var text = item." + displayField + ";");
            var selected ="";
            if(selectedValue == value)
                selected = "selected";
            var html = "<option value='" + value + "' " + selected + ">" + text + "</option>";
            $("#" + id).append(html);
        }
    }
};