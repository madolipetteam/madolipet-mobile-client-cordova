var HeadPage = Class( MadolPage, {

    constructor: function()
    {
        HeadPage.$super.call(this, "HeadPage");
    }
    ,
    initPage: function(me)
    {
        console.log("wise : HeadPage.initPage()");
        console.log("divMadolHeader.width : " + $(screen).width());
        $("#divMadolHeader").css("width", $(screen).width() );
        me.initProfile();
        me.initOrgTitle();
    }
    ,
    initProfile: function()
    {
        var user = GLOBAL.loginUser;
        console.log("HeadPage.user login:");
        console.log(user);
        $("#divProfile").attr("src", user.ProfilePic);
    }
    ,
    initOrgTitle: function()
    {
        var user = GLOBAL.loginUser;
        $("#divSmuTitle").html(user.Organization_OrganizationName);
    }
    
});