var HomePage = Class( MadolPage, {
	constructor: function ()
	{
        HomePage.$super.call(this, "HomePage");
	}
	,
	initPage: function(me)
	{
		console.log("wise : HomePage.initPage()");
        me.runEventHandler(this, "get-and-display-forums" );

        var cmbForums = $("#cmbForums");
        cmbForums.on("change", function()
        {
        	var idForum = me.getSelectedForum();
            me.runEventHandler(me, "get-and-display-articles", idForum);
        });
	}
	,
	getSelectedForum: function()
	{
		var value = $("#cmbForums").find("option:selected").val();
		return value;
	}
	,
	displayForums: function(forums)
	{
	    var cmbForums = $("#cmbForums");
	    Util.populateCombo("cmbForums", forums, "Forum_IdForum", "Forum_ForumName");
	    //var idForum = this.getSelectedForum();
        //this.runEventHandler(this, "get-and-display-articles", idForum);
	}
	,
	displayArticles: function(articles)
	{
		var sListItem = $("#listOfArticles").html();
		var list = $("#listOfArticles");
		for(var i = 0; i < articles.length; i++)
		{
			var article = articles[i];
			var sItem = sListItem.replace("{ArticleMainPicture}", article.ArticleMainPicture);
			sItem		= sItem.replace("{ArticleTitle}", article.ArticleTitle);
			sItem		= sItem.replace("{ArticleShortDesc}", article.ArticleShortDesc);
			list.append(sItem);
		}
	}
    
});