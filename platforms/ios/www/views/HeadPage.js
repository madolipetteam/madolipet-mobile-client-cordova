var HeadPage = Class( MadolPage, {

    constructor: function()
    {
        HeadPage.$super.call(this, "HeadPage");
    }
    ,
    initPage: function(me)
    {
        me.initProfile();
        me.initOrgTitle();
    }
    ,
    initProfile: function()
    {
        var user = GLOBAL.loginUser;
        $("#divProfile").attr("src", user.ProfilePic);
    }
    ,
    initOrgTitle: function()
    {
        var user = GLOBAL.loginUser;
        $("#divSmuTitle").html(user.Organization_OrganizationName);
    }
    
});