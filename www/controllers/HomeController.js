var HomeController = Class(HeadController, {
    homePage: null,
    constructor: function()
    {
        HomeController.$super.call(this);
        
        //Create HomePage object
        this.homePage = new HomePage();
        this.homePage.caller = this;
        this.homePage.eventHandler = this.homePageAction;
    }
    ,
    afterLogin: function(me)
    {
        //Run HeadController's afterlogin
        HomeController.$superp.afterLogin.call(me, me);
        me.homePage.display();
    }
    ,
    homePageAction: function(me, sender, command, param)
    {
        switch(command)
        {
                
        }
    }
});