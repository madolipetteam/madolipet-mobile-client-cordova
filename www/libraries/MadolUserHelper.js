var MadolUserHelper =
{
    login: function(o, callback)
    {
        var url = Config.getUrl("MadolUserExt.login", o);
        Util.get(url, null, function(data)
        {
            if(callback != null)
                callback(data.items);
        });
    }
    ,
    getCurrentLogin: function(callback)
    {
        var url = Config.getUrl("MadolUserExt.getCurrentLogin");
        Util.get(url, null, function(data)
        {
            if(callback != null)
                callback(data.items);
        });
    }
}