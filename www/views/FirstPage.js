var FirstPage = Class(PageHandler, {
    constructor: function()
    {
        FirstPage.$super.call(this, "FirstPage");
    }
    ,
    //initPage() is function called after display
    initPage: function(me)
    {
        //Display this page for 2 seconds
        setTimeOut(2000, function(){
            
            //Call HomeController
            var homeController = new HomeController();
            homeController.start();
        });
    }
    
});