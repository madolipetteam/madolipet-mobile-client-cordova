var LoginPage = Class(MadolPage, {
    pg: null,
    constructor: function()
    {
        LoginPage.$super.call(this, "LoginPage");
        var me = this;
        /**/
    }
    ,
    initPage: function(me)
    {
        $("#btnLogin").click(function()
            {
                var loginO = me.getLoginObject(me);
                me.runEventHandler(this, 'login', loginO);
            });
    }
    ,
    getLoginObject: function(me)
    {
        var o = me.getDataObject("divLogin");
        return o;
    }


});